from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel

from guildownersettings.models import GuildOwnerSettings


ROLE_TYPES = (
    (
        "gender",
        "Gender",
    ),
    (
        "notif",
        "Notifications",
    ),
    (
        "comfort",
        "Comfort",
    ),
)


class AutoRoles(models.Model):
    guild = models.OneToOneField(GuildOwnerSettings, on_delete=models.CASCADE)
    emoji = models.CharField(max_length=10)
    role_id = models.BigIntegerField()
    role_type = models.CharField(max_length=10, choices=ROLE_TYPES)

    def __str__(self):
        return f"AutoRole<{self.emoji},{self.role_id},{self.role_type}> for guild: {self.guild.guild_id}"

    panel = [
        FieldPanel("emoji"),
        FieldPanel("role_id"),
        FieldPanel("role_type"),
    ]
