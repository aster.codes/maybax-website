from django.apps import AppConfig


class AutorolesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'autoroles'
