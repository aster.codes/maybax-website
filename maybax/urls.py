from django.conf import settings
from django.urls import include, path
from django.contrib import admin

from django.views.generic.base import TemplateView

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from search import views as search_views

urlpatterns = [
    path("django-admin/", admin.site.urls),
    path("documents/", include(wagtaildocs_urls)),
    path("search/", search_views.search, name="search"),
    path("accounts/", include("allauth.urls")),
    path("editor/", include(wagtailadmin_urls)),
]

from django.shortcuts import render


def custom_error_404(request, exception=None):
    return render(request, "404.html", {})


def custom_error_403(request, exception=None):
    return render(request, "403.html", {})


def custom_error_500(request):
    return render(request, "500.html", {})


urlpatterns += [
    path("sample_404/", custom_error_404),
    path("sample_403/", custom_error_403),
    path("sample_500/", custom_error_500),
]

if settings.DEBUG:

    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    # Serve static and media files from development server
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns = urlpatterns + [
    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    path("", include(wagtail_urls)),
    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    path("pages/", include(wagtail_urls)),
]
