from django.db import models
from wagtail_blocks.blocks import (
    HeaderBlock,
    ListBlock,
    ImageTextOverlayBlock,
    CroppedImagesWithTextBlock,
    ListWithImagesBlock,
    ThumbnailGalleryBlock,
    ChartBlock,
    MapBlock,
    ImageSliderBlock,
)
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.search import index
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import (
    FieldPanel,
    MultiFieldPanel,
    InlinePanel,
    StreamFieldPanel,
)
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtailmetadata.models import MetadataPageMixin


from wagtail.core.models import Page


class HomePage(Page):
    pass


class StandardPage(MetadataPageMixin, Page):
    leadin = models.CharField(max_length=255, null=True, blank=True)
    header = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    hero_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )

    body = StreamField(
        [
            ("header", HeaderBlock()),
            ("paragraph", blocks.RichTextBlock()),
            ("table", TableBlock()),
            ("list", ListBlock()),
            ("image_text_overlay", ImageTextOverlayBlock()),
            ("cropped_images_with_text", CroppedImagesWithTextBlock()),
            ("list_with_images", ListWithImagesBlock()),
            ("thumbnail_gallery", ThumbnailGalleryBlock()),
            ("chart", ChartBlock()),
            ("map", MapBlock()),
            ("image_slider", ImageSliderBlock()),
        ],
        blank=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("leadin"),
        FieldPanel("header"),
        FieldPanel("description"),
        ImageChooserPanel("hero_image"),
        StreamFieldPanel("body"),
    ]
    search_fields = Page.search_fields + [
        index.SearchField("title"),
        index.SearchField("leadin"),
        index.SearchField("header"),
        index.SearchField("description"),
    ]
    parent_page_types = ["home.HomePage", "home.StandardPage"]
