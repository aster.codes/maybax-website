from wagtail.contrib.modeladmin.options import (
    ModelAdmin,
    ModelAdminGroup,
    modeladmin_register,
)
from .models import GuildOwnerSettings

from autoroles.wagtail_hooks import AutorolesAdmin
from system.wagtail_hooks import GuildSystemSettingsAdmin


class GuildOwnerSettingsAdmin(ModelAdmin):
    model = GuildOwnerSettings
    menu_label = "Guild Settings"
    menu_icon = "folder-open-inverse"
    menu_order = 100
    add_to_settings_menu = False
    exclude_from_explorer = False


class GuildOwnerGroup(ModelAdminGroup):
    menu_label = "Guild Owner Area"
    menu_icon = "folder-open-inverse"
    menu_order = 300
    items = (
        GuildOwnerSettingsAdmin,
        GuildSystemSettingsAdmin,
        AutorolesAdmin,
    )


modeladmin_register(GuildOwnerGroup)
