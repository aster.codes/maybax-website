from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel


class GuildOwnerSettings(models.Model):
    guild_id = models.BigIntegerField(unique=True)
    logging_channel = models.BigIntegerField(null=True, blank=True)
    vc_logging_channel = models.BigIntegerField(null=True, blank=True)
    mod_logging_channel = models.BigIntegerField(null=True, blank=True)

    def __str__(self):
        return f"Guild Id [{self.guild_id}]"

    panel = [
        FieldPanel("guild_id"),
        MultiFieldPanel(
            [
                FieldPanel("logging_channel"),
                FieldPanel("vc_logging_channel"),
                FieldPanel("mod_logging_channel"),
            ],
            heading="Logging Channels",
            classname="collapsible collapsed",
        ),
    ]
