from django.apps import AppConfig


class GuildownersettingsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'guildownersettings'
