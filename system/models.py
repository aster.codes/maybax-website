import logging

from django.db import models
from django.db.models import JSONField
from timezone_field import TimeZoneField
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
import os
import redis

from guildownersettings.models import GuildOwnerSettings

logger = logging.getLogger()


class GuildSystemSettings(models.Model):
    guild = models.OneToOneField(
        GuildOwnerSettings, on_delete=models.CASCADE, unique=True
    )
    system_role = models.BigIntegerField()

    def __str__(self):
        return f"Guild Settings for {self.guild.guild_id}"

    panels = [
        MultiFieldPanel(
            [FieldPanel("system_role"), FieldPanel("guild")],
            heading="System Proxy Role",
        )
    ]


class System(models.Model):
    pluralkit_id = models.CharField(max_length=20, blank=True, null=True, unique=True)
    tupper_id = models.CharField(max_length=20, blank=True, null=True, unique=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(max_length=4096, blank=True, null=True)
    tag = models.CharField(max_length=255, blank=True, null=True)
    avatar_url = models.TextField(blank=True, null=True)
    timezone = TimeZoneField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    owner_discord_id = models.CharField(
        max_length=255, unique=True, blank=True, null=True
    )

    def save(self, *args, **kwargs):
        if not self.pluralkit_id:
            self.pluralkit_id = None
        if not self.tupper_id:
            self.tupper_id = None

        super().save(*args, **kwargs)

    def __str__(self):
        return f"System: {self.name}"

    panels = [
        MultiFieldPanel(
            [
                FieldPanel("name"),
                FieldPanel("description"),
                FieldPanel("tag"),
                FieldPanel("avatar_url"),
            ],
            heading="System Information",
        ),
        MultiFieldPanel(
            [
                FieldPanel("timezone"),
            ],
            heading="System Meta Data",
            classname="collapsible collapsed",
        ),
        MultiFieldPanel(
            [
                FieldPanel("pluralkit_id"),
                FieldPanel("tupper_id"),
                FieldPanel("owner_discord_id"),
            ],
            heading="Integrations Data",
            classname="collapsible collapsed",
        ),
    ]


class Group(models.Model):
    pluralkit_id = models.CharField(max_length=20, blank=True, null=True, unique=True)
    tupper_id = models.CharField(max_length=20, blank=True, null=True, unique=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(max_length=4096, blank=True, null=True)
    tag = models.TextField(max_length=255, blank=True, null=True)
    avatar_url = models.TextField(blank=True, null=True)
    timezone = TimeZoneField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    system = models.ForeignKey(System, on_delete=models.CASCADE, related_name="_groups")

    def __str__(self):
        return f"{self.name}"

    def save(self, *args, **kwargs):
        if not self.pluralkit_id:
            self.pluralkit_id = None
        if not self.tupper_id:
            self.tupper_id = None

        super().save(*args, **kwargs)

    panels = [
        MultiFieldPanel(
            [
                FieldPanel("name"),
                FieldPanel("description"),
                FieldPanel("tag"),
                FieldPanel("avatar_url"),
            ],
            heading="Group Information",
        ),
        MultiFieldPanel(
            [
                FieldPanel("timezone"),
            ],
            heading="Group Meta Data",
            classname="collapsible collapsed",
        ),
        FieldPanel("system"),
        MultiFieldPanel(
            [
                FieldPanel("pluralkit_id"),
                FieldPanel("tupper_id"),
            ],
            heading="Integrations Data",
            classname="collapsible collapsed",
        ),
    ]


class Member(models.Model):
    pluralkit_id = models.CharField(max_length=20, blank=True, null=True, unique=True)
    tupper_id = models.CharField(max_length=20, blank=True, null=True, unique=True)
    name = models.CharField(max_length=255)
    tag = models.CharField(max_length=255, blank=True, null=True)
    indicators = JSONField(blank=True, null=True)
    pronouns = models.TextField(blank=True, null=True)
    color = models.CharField(
        max_length=6, blank=True, null=True, help_text="Hex Codes accepted."
    )
    avatar_url = models.TextField(blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    description = models.TextField(max_length=4096, blank=True, null=True)
    public = models.BooleanField(default=False)
    position = models.CharField(max_length=255, blank=True, null=True)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    tupper_group_id = models.CharField(max_length=20, blank=True, null=True)
    tupper_group_position = models.CharField(max_length=20, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    groups = models.ForeignKey(
        Group, on_delete=models.CASCADE, related_name="_members", null=True, blank=True
    )
    system = models.ForeignKey(
        System, on_delete=models.CASCADE, related_name="_members"
    )

    def __str__(self):
        return f"{self.name}"

    def save(self, *args, **kwargs):
        redis_system_key = None
        redis_member_key = None
        if not self.pluralkit_id:
            self.pluralkit_id = None
        if not self.tupper_id:
            self.tupper_id = None

        super().save(*args, **kwargs)
        discord_id = self.system.owner_discord_id
        redis_member_index_set = f"{discord_id}:member_list"
        redis_member_key = f"{discord_id}:{self.id}:member"
        redisc = redis.Redis(
            host=f"{os.environ.get('REDIS_HOST')}",
            port=int(os.environ.get("REDIS_PORT")),
        )
        logger.error(redisc.hgetall(redis_member_key))
        logger.error(redisc.smembers(redis_member_index_set))
        if not self.system.owner_discord_id:
            return
        if self.id not in redisc.smembers(redis_member_index_set) or []:
            redisc.sadd(redis_member_index_set, self.id)
        redisc.hset(name=redis_member_key, key="name", value=str(self.name) or "")
        logger.info(self.indicators)
        indicators = []
        for ind in self.indicators:
            if not ind["prefix"]:
                ind["prefix"] = "[NONE]"
            if not ind["suffix"]:
                ind["suffix"] = "[NONE]"
            indicators.append(
                f"prefix;{ind['prefix'].strip()},suffix;{ind['suffix'].strip()}"
            )
        redisc.hset(
            name=redis_member_key,
            key="indicators",
            value="|".join(indicators),
        )
        redisc.hset(
            name=redis_member_key, key="description", value=str(self.description or "")
        )
        redisc.hset(
            name=redis_member_key,
            key="pluralkit_id",
            value=str(self.pluralkit_id or ""),
        )
        redisc.hset(name=redis_member_key, key="system_id", value=str(self.system.id))
        redisc.hset(name=redis_member_key, key="color", value=str(self.color or ""))
        redisc.hset(
            name=redis_member_key,
            key="tupper_group_pos",
            value=str(self.tupper_group_position or ""),
        )
        redisc.hset(
            name=redis_member_key,
            key="groups_id",
            value=str(self.tupper_group_id or ""),
        )
        redisc.hset(
            name=redis_member_key, key="pronouns", value=str(self.pronouns or "")
        )
        redisc.hset(
            name=redis_member_key,
            key="display_name",
            value=str(self.display_name or ""),
        )
        redisc.hset(
            name=redis_member_key,
            key="created_at",
            value=str(
                self.created_at.astimezone(self.system.timezone).isoformat() or ""
            ),
        )
        redisc.hset(
            name=redis_member_key,
            key="birthday",
            value=str(
                self.birthday.isoformat() if self.birthday else "",
            ),
        )
        redisc.hset(
            name=redis_member_key, key="tupper_id", value=str(self.tupper_id or "")
        )
        redisc.hset(
            name=redis_member_key, key="public", value=str(self.public or False)
        )
        redisc.hset(
            name=redis_member_key,
            key="avatar_url",
            value=str(
                self.avatar_url
                or "https,//cdn.discordapp.com/attachments/857689194415390720/914757550656872468/4PE-nlL_PdMD5PrFNLnjurHQ1QKPnCvg368LTDnfM-M.png",
            ),
        )
        redisc.hset(
            name=redis_member_key, key="position", value=str(self.position or "")
        )
        redisc.hset(name=redis_member_key, key="id_", value=str(self.id))
        redisc.hset(
            name=redis_member_key,
            key="tupper_group_id",
            value=str(self.tupper_group_id or ""),
        )
        redisc.hset(name=redis_member_key, key="tag", value=str(self.tag or ""))

    panels = [
        MultiFieldPanel(
            [
                FieldPanel("name"),
                FieldPanel("tag"),
                FieldPanel("display_name"),
                FieldPanel("description"),
                FieldPanel("birthday"),
                FieldPanel("pronouns"),
                FieldPanel("color"),
                FieldPanel("avatar_url"),
                FieldPanel("public"),
            ],
            heading="Member Information",
        ),
        FieldPanel("system"),
        FieldPanel("groups"),
        FieldPanel("indicators"),
        MultiFieldPanel(
            [
                FieldPanel("pluralkit_id"),
                FieldPanel("tupper_id"),
            ],
            heading="Integrations Data",
            classname="collapsible collapsed",
        ),
    ]
