from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User, Group


@receiver(models.signals.post_save, sender=User)
def post_save_user_signal_handler(sender, instance, created, **kwargs):
    if created:
        instance.is_staff = True
        group = Group.objects.get(name="System")
        instance.groups.add(group)
        instance.save()
