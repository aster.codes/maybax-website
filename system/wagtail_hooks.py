from wagtail.contrib.modeladmin.options import (
    ModelAdmin,
    ModelAdminGroup,
    modeladmin_register,
)

# from django.contrib.admin import BooleanFieldListFilter
from django.http import Http404
from django.db import models
from django_json_widget.widgets import JSONEditorWidget
from .models import Member, System, Group, GuildSystemSettings
from admin_list_controls.views import ListControlsIndexView
from admin_list_controls.components import Button, Panel, Columns, Block
from admin_list_controls.selectors import SortSelector
from admin_list_controls.actions import TogglePanel

from admin_list_controls.actions import SubmitForm
from admin_list_controls.filters import TextFilter


class SystemIndexView(ListControlsIndexView):
    def build_list_controls(self):
        return [
            Block()(
                Button(action=TogglePanel(ref="sorting"))("Sorting"),
                Button(action=TogglePanel(ref="main_panel"))("Search Name/Tag"),
                Button(action=TogglePanel(ref="description_panel"))(
                    "Search Description"
                ),
                Button(action=TogglePanel(ref="meta_panel"))("Search Meta"),
            ),
            Panel(collapsed=True, ref="sorting")(
                SortSelector(
                    value="name_sort_asc",
                    is_default=True,
                    apply_to_queryset=lambda queryset: queryset.order_by("name"),
                )("Sort by name A-Z"),
                SortSelector(
                    value="name_sort_desc",
                    apply_to_queryset=lambda queryset: queryset.order_by("-name"),
                )("Sort by name Z-A"),
            ),
            Panel(ref="main_panel")(
                Columns()(
                    TextFilter(
                        name="name",
                        label="System Name",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            name__icontains=value
                        ),
                    ),
                    TextFilter(
                        name="tag",
                        label="System Tag",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            tag__icontains=value
                        ),
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
            Panel(collapsed=True, ref="description_panel")(
                TextFilter(
                    name="description",
                    label="System Description",
                    apply_to_queryset=lambda queryset, value: queryset.filter(
                        description__icontains=value
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
            Panel(collapsed=True, ref="meta_panel")(
                Columns()(
                    TextFilter(
                        name="pluralkit_id",
                        label="System PluralKit ID",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            pluralkit_id__icontains=value
                        ),
                    ),
                    TextFilter(
                        name="tupper_id",
                        label="System Tupperbox ID",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            tupper_id__icontains=value
                        ),
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
        ]


class SystemAdmin(ModelAdmin):
    model = System
    index_view_class = SystemIndexView
    menu_label = "System"  # ditch this to use verbose_name_plural from model
    menu_icon = "pilcrow"  # change as required
    menu_order = 000  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        owner_discord_id = request.user.socialaccount_set.all()[0].uid
        return queryset.filter(owner_discord_id=owner_discord_id)


class GroupIndexView(ListControlsIndexView):
    def build_list_controls(self):
        return [
            Block()(
                Button(action=TogglePanel(ref="sorting"))("Sorting"),
                Button(action=TogglePanel(ref="main_panel"))("Search Name/Tag"),
                Button(action=TogglePanel(ref="description_panel"))(
                    "Search Description"
                ),
                Button(action=TogglePanel(ref="meta_panel"))("Search Meta"),
            ),
            Panel(collapsed=True, ref="sorting")(
                SortSelector(
                    value="name_sort_asc",
                    is_default=True,
                    apply_to_queryset=lambda queryset: queryset.order_by("name"),
                )("Sort by name A-Z"),
                SortSelector(
                    value="name_sort_desc",
                    apply_to_queryset=lambda queryset: queryset.order_by("-name"),
                )("Sort by name Z-A"),
            ),
            Panel(ref="main_panel")(
                Columns()(
                    TextFilter(
                        name="name",
                        label="Group Name",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            name__icontains=value
                        ),
                    ),
                    TextFilter(
                        name="tag",
                        label="Group Tag",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            tag__icontains=value
                        ),
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
            Panel(collapsed=True, ref="description_panel")(
                TextFilter(
                    name="description",
                    label="Group Description",
                    apply_to_queryset=lambda queryset, value: queryset.filter(
                        description__icontains=value
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
            Panel(collapsed=True, ref="meta_panel")(
                Columns()(
                    TextFilter(
                        name="pluralkit_id",
                        label="Group PluralKit ID",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            pluralkit_id__icontains=value
                        ),
                    ),
                    TextFilter(
                        name="tupper_id",
                        label="Group Tupperbox ID",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            tupper_id__icontains=value
                        ),
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
        ]


class GroupAdmin(ModelAdmin):
    model = Group
    index_view_class = GroupIndexView
    menu_label = "System Groups"  # ditch this to use verbose_name_plural from model
    menu_icon = "pilcrow"  # change as required
    menu_order = 100  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name", "tag", "created_at")
    list_filter = ("tag",)
    search_fields = ("name", "tag", "description")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        owner_discord_id = request.user.socialaccount_set.all()[0].uid
        return queryset.filter(system__owner_discord_id=owner_discord_id)


class MemberIndexView(ListControlsIndexView):
    def build_list_controls(self):
        return [
            Block()(
                Button(action=TogglePanel(ref="sorting"))("Sorting"),
                Button(action=TogglePanel(ref="main_panel"))("Search Name/Tag"),
                Button(action=TogglePanel(ref="description_panel"))(
                    "Search Description"
                ),
                Button(action=TogglePanel(ref="groups_search"))("Search by Groups"),
                Button(action=TogglePanel(ref="meta_panel"))("Search Meta"),
            ),
            Panel(collapsed=True, ref="sorting")(
                SortSelector(
                    value="name_sort_asc",
                    is_default=True,
                    apply_to_queryset=lambda queryset: queryset.order_by("name"),
                )("Sort by name A-Z"),
                SortSelector(
                    value="name_sort_desc",
                    apply_to_queryset=lambda queryset: queryset.order_by("-name"),
                )("Sort by name Z-A"),
                SortSelector(
                    value="groups_name_sort_asc",
                    is_default=True,
                    apply_to_queryset=lambda queryset: queryset.order_by(
                        "groups__name"
                    ),
                )("Sort by Group Name A-Z"),
                SortSelector(
                    value="groups_name_z_to_a",
                    apply_to_queryset=lambda queryset: queryset.order_by(
                        "-groups__name"
                    ),
                )("Sort by Group Name Z-A"),
            ),
            Panel(ref="main_panel")(
                Columns()(
                    TextFilter(
                        name="name",
                        label="Member Name",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            name__icontains=value
                        ),
                    ),
                    TextFilter(
                        name="tag",
                        label="Member Tag",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            tag__icontains=value
                        ),
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
            Panel(collapsed=True, ref="description_panel")(
                TextFilter(
                    name="description",
                    label="Member Description",
                    apply_to_queryset=lambda queryset, value: queryset.filter(
                        description__icontains=value
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
            Panel(collapsed=True, ref="groups_search")(
                Columns()(
                    TextFilter(
                        name="group_name",
                        label="Group Name",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            groups__name__icontains=value
                        ),
                    ),
                    TextFilter(
                        name="group_description",
                        label="Group Description",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            gorups__description__icontains=value
                        ),
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
            Panel(collapsed=True, ref="meta_panel")(
                Columns()(
                    TextFilter(
                        name="pluralkit_id",
                        label="Member PluralKit ID",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            pluralkit_id__icontains=value
                        ),
                    ),
                    TextFilter(
                        name="tupper_id",
                        label="Member Tupperbox ID",
                        apply_to_queryset=lambda queryset, value: queryset.filter(
                            tupper_id__icontains=value
                        ),
                    ),
                ),
                Button(action=SubmitForm())(
                    "Apply filters",
                ),
            ),
        ]


class MemberAdmin(ModelAdmin):
    model = Member
    index_view_class = MemberIndexView
    menu_label = "System Members"  # ditch this to use verbose_name_plural from model
    menu_icon = "placeholder"  # change as required
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    list_display = [
        "name",
        "tag",
        "pronouns",
        "display_name",
        "created_at",
        "groups",
    ]
    # list_filter = ('groups', BooleanFieldListFilter)
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    formfield_overrides = {
        models.JSONField: {"widget": JSONEditorWidget},
    }

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        owner_discord_id = request.user.socialaccount_set.all()[0].uid
        return queryset.filter(system__owner_discord_id=owner_discord_id)


class SystemGroup(ModelAdminGroup):
    menu_label = "System"
    menu_icon = "folder-open-inverse"  # change as required
    menu_order = 300
    items = (
        SystemAdmin,
        GroupAdmin,
        MemberAdmin,
    )


modeladmin_register(SystemGroup)


class GuildSystemSettingsAdmin(ModelAdmin):
    model = GuildSystemSettings
    menu_label = "Guild System Proxy Settings"
    menu_icon = "placeholder"
    menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
